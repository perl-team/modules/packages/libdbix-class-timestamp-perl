libdbix-class-timestamp-perl (0.14-3) unstable; urgency=medium

  [ Laurent Baillet ]
  * fix lintian file-contains-trailing-whitespace warning

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Repository.
  * Add missing build dependency on libmodule-install-perl.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Update standards version to 4.1.5, no changes needed.
  * Bump debhelper from old 12 to 13.

  [ gregor herrmann ]
  * Remove generated test file via debian/clean. (Closes: #1047329)
  * Update debian/upstream/metadata.
  * Declare compliance with Debian Policy 4.6.2.
  * Annotate test-only build dependencies with <!nocheck>.
  * Update years of packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Tue, 05 Mar 2024 15:47:31 +0100

libdbix-class-timestamp-perl (0.14-2) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Brian Cassidy from Uploaders. Thanks for your work!
  * Add Testsuite declaration for autopkgtest-pkg-perl.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Niko Tyni ]
  * Update to debhelper compat level 10
  * Update to Standards-Version 4.1.4
  * Declare that the package does not need (fake)root to build

 -- Niko Tyni <ntyni@debian.org>  Sat, 07 Apr 2018 21:17:08 +0300

libdbix-class-timestamp-perl (0.14-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Changed: Replace versioned (build-)dependency on
    perl (>= 5.6.0-{12,16}) with an unversioned dependency on perl (as
    permitted by Debian Policy 3.8.3).
  * Email change: Salvatore Bonaccorso -> carnil@debian.org
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ Ansgar Burchardt ]
  * New upstream release (0.14). Closes: #636268
  * Bump build-dep on libdatetime-perl to >= 2:0.5500.
  * debian/copyright: Formatting changes; refer to "Debian systems" instead of
    "Debian GNU/Linux systems"; refer to /usr/share/common-licenses/GPL-1.
  * Use source format 3.0 (quilt).
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/copyright: update debian/* stanza.
  * Switch to debhelper compatibility level 8.
  * Set Standards-Version to 3.9.2 (no changes).
  * Set urgency to medium (RC bug fix, perl 5.14 transition).

 -- gregor herrmann <gregoa@debian.org>  Fri, 18 Nov 2011 16:23:06 +0100

libdbix-class-timestamp-perl (0.12-1) unstable; urgency=low

  * New upstream release.
  * debian/control: remove build dependency on libsql-translator-perl, not
    needed any more.

 -- gregor herrmann <gregoa@debian.org>  Wed, 01 Jul 2009 17:32:04 +0200

libdbix-class-timestamp-perl (0.11-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * New upstream release
  * Update debian/copyright for debian/* packaging.

  [ gregor herrmann ]
  * Remove Module::Install hack from debian/rules, bump versioned dependency
    on debhelper in debian/control.

 -- gregor herrmann <gregoa@debian.org>  Fri, 26 Jun 2009 16:58:29 +0200

libdbix-class-timestamp-perl (0.08-1) unstable; urgency=medium

  [ gregor herrmann ]
  * New upstream release.
  * Remove whatis.patch (included upstream) and patch framework.
  * debian/copyright: fix typo in years of copyright for files in inc/*.
  * Add /me to Uploaders.
  * debian/rules: set PERL5_CPANPLUS_IS_RUNNING to keep Module::Install from
    loading CPAN.pm.

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Salvatore Bonaccorso ]
  * Bump Standards Version to 3.8.2 (no changes)
  * Add Build-Depends on libdatetime-format-sqlite-perl (Closes: #533949).
  * Add myself to Uploaders
  * Minimize debian/rules using override_ targets.

 -- Salvatore Bonaccorso <salvatore.bonaccorso@gmail.com>  Sun, 21 Jun 2009 18:26:49 +0200

libdbix-class-timestamp-perl (0.07-1) unstable; urgency=low

  * Initial Release. (Closes: #525147)

 -- Brian Cassidy <brian.cassidy@gmail.com>  Thu, 23 Apr 2009 16:23:39 -0300
